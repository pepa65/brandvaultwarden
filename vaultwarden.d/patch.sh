# patch.sh - Patch files affecting branding

# Usage: `. patch.sh`  # Is automatically run on startup of the container
#   Script and images reside inside the container in /etc/vaultwarden.d/
# Required: sed grep coreutils(cp) apk[Alpine]

name="<a href=\"https://gitlab.com/pepa65/brandvaultwarden\">CRICS</a>"

e=/etc/vaultwarden.d
logo=logo48-10.png
w=web-vault
wi=$w/images
wii=$wi/icons
wir=$wi/register-layout

Cp(){ # $1:image $2:dir
	test -f "$1" && test -d "$2" && cp "$1" "$2"
}

# Copying images
Cp "$e/favicon.ico" "$w/"
Cp "$e/logo-dark@2x.png" "$wi/"
Cp "$e/logo-white@2x.png" "$wi/"
Cp "$e/logo-horizontal-white.png" "$wi/"
Cp "$e/logo-horizontal-white.png" "$wir/"
Cp "$e/android-chrome-192x192.png" "$wii/"
Cp "$e/android-chrome-512x512.png" "$wii/"
Cp "$e/apple-touch-icon.png" "$wii/"
Cp "$e/favicon-16x16.png" "$wii/"
Cp "$e/favicon-32x32.png" "$wii/"
Cp "$e/mstile-150x150.png" "$wii/"
Cp "$e/safari-pinned-tab.svg" "$wii/"
Cp "$e/$logo" "$wii/"

# Replace fa-shield 'character' by crics logo
# Corresponds to brandpatch's logo=
css=$(echo /web-vault/app/main.*.css)
test -f "$e/$logo" && test -f "$css" && sed -i 's@""@url(../images/icons/'"$logo"')@' "$css"
# Highlight Filter
if ! grep -q 'margin-left: -30px' "$css"
then test -f "$css" && sed -i 's@app-vault-groupings .card li.active > a.*@&\n  padding: 0 8px 0 26px; margin-left: -30px; border-bottom: 2px solid #175DDC; border-top: 2px solid #175DDC;@' "$css"
fi

# Replace page title
js=$(echo /web-vault/app/main.*.js)
test -f "$js" && sed -i 's@"Bitwarden"@"'$name' Bitwarden"@g' "$js"
test -f "$js" && sed -i 's@, Bitwarden Inc.@ Bitwarden Inc. served by '"$name"'@g' "$js"

# Replace organization by group
en=/web-vault/locales/en/messages.json
sed -i 's@n organization@ group@g' "$en"
sed -i 's@ organization@ group@g' "$en"
sed -i 's@ Organization@ Group@g' "$en"
sed -i 's@Organization @Group @g' "$en"
sed -i 's@"Organization@"Group@g' "$en"
sed -i 's@organization @group @g' "$en"

# Set Dark-Mode as default
#sed -i 's#class="theme_light"#class="theme_dark"#' $w/index.html
#sed -i 's#class="theme_light"#class="theme_dark"#' $w/sso-connector.html

# Update ADMIN_TOKEN in case /data/config.json was already present
test -n "$ADMIN_TOKEN" && sed -i '/"admin_token"/ s="[^"]*",$="'"$ADMIN_TOKEN"'",=' /data/config.json
