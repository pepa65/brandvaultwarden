# brandvaultwarden

**The branding in this repo is for CRICS, but can be adjusted as required.**

Adjust the branding by replacing the images in `vaultwarden.d`. If images are removed, the default ones are used.

## Just patch a running server
* It should source the patch script `/etc/vaultwarden.d/patch.sh` on bootup. This can be run multiple times without harm.
* If necessary, access the container with: `docker exec -it Vaultwarden /bin/sh`
* Edit /data/config.json for the admin page password (hash it with `vaultwarden hash`)

## Set up a full branding-patched Vaultwarden server
* Required: `git` `apt-get` `systemd`(`systemctl`)
* Download the repo files by: `git clone https://gitlab.com/pepa65/brandvaultwarden`
* Change into the repo directory by: `cd brandvaultwarden`
* Run `bash install` to copy `_vars` to `vars` (or copy it manually)
* Edit the variables in `vars`
* Run `bash install`
